from pprint import pprint

from math import isnan
import threading
from logging import Handler
from logging import Formatter
from requests import put
from httplib2 import Http

from oauth2client import file, client, tools
from apiclient.discovery import build
from googleapiclient.errors import HttpError

try:
    import numpy
    has_numpy = True
except ImportError:
    has_numpy = False

class AtomicIdx:
    def __init__(self):
        ## A - 1 = 64
        self.column = 64
        self.running_col = self.column

    def reset(self):
        self.running_col = self.column

    def commit(self):
        self.column = self.running_col

    def next(self):
        self.running_col += 1
        ## Z = 90
        if self.running_col > 90:
            ## A = 65
            self.running_col = 65
        return chr(self.running_col)

class GridLogHandler(Handler):
    def __init__(self, sheetid, gapi_client_secret):
        super().__init__()
        if sheetid == '' or gapi_client_secret == '':
            raise ValueError('empty string passed to gridlogger')
        self.sheetid = sheetid
        SCOPES = ['https://www.googleapis.com/auth/spreadsheets','https://www.googleapis.com/auth/drive','https://www.googleapis.com/auth/drive.file']
        store = file.Storage('credentials.json')
        creds = store.get()
        if not creds or creds.invalid:
            flow = client.flow_from_clientsecrets(gapi_client_secret, SCOPES)
            creds = tools.run_flow(flow, store)
        self.gservice = build('sheets', 'v4', http=creds.authorize(Http()))
        self.atomic_colidx = {}

    def clear_ranges(self, data):
        body = { "ranges": data }
        request = self.gservice.spreadsheets().values().batchClear(spreadsheetId=self.sheetid, body=body)
        response = request.execute()

    def update_sheet(self, data):
        body = { "valueInputOption": "RAW", "includeValuesInResponse": False, "responseDateTimeRenderOption": "SERIAL_NUMBER", "responseValueRenderOption": "FORMATTED_VALUE" }
        body['data'] = data
        request = self.gservice.spreadsheets().values().batchUpdate(spreadsheetId=self.sheetid, body=body)
        response = request.execute()

    def log_first_then_retry(self, record, depth):
        try:
            data, clears = self.format(record)
            if data is None:
                pprint('sth wrong with logger arguments')
                return
            self.clear_ranges(clears)
            self.update_sheet(data)
        except HttpError as ex:
            if 404 == ex.resp.status and '"Requested entity was not found' in str(ex):
                pprint('likely wrong sheetid')
                return
            if 400 == ex.resp.status and 'Invalid range' in str(ex):
                if depth > 0:
                    ## avoid stackoverflow only
                    return
                ## guess sheet not found
                sheet_metadata = self.gservice.spreadsheets().get(spreadsheetId=self.sheetid).execute()
                sheets = sheet_metadata.get('sheets', [])
                ## make sure the sheet does not exist
                for sheet in sheets:
                    props = sheets[0].get("properties", {})
                    title = props.get("title", "Whatever")
                    if title == record.name:
                        pprint('logging.py, this case is not handled, no logging action')
                        return
                ## create the sheet and retry
                body = {'requests':[{'addSheet':{'properties':{'title': record.name}}}]}
                request = self.gservice.spreadsheets().batchUpdate(spreadsheetId = self.sheetid, body = body)
                response = request.execute()
                return self.log_first_then_retry(record, depth + 1)
            import sys
            exc_type, exc_value, exc_traceback = sys.exc_info()
            pprint('out print error {} {} {}'.format(exc_traceback.tb_lineno, exc_value, exc_type))
        except:
            import sys
            exc_type, exc_value, exc_traceback = sys.exc_info()
            pprint('out print error {} {} {}'.format(exc_traceback.tb_lineno, exc_value, exc_type))

    def emit(self, record):
        if record.name == "root":
            pprint("must give a name to logger, root is not allowed")
            return
        try:
            if record.name not in self.atomic_colidx:
                self.atomic_colidx[record.name] = AtomicIdx()
            record.idx_provider = self.atomic_colidx[record.name]
            self.log_first_then_retry(record, 0)
            self.atomic_colidx[record.name].commit()
        except:
            import sys
            exc_type, exc_value, exc_traceback = sys.exc_info()
            pprint('out print error {} {} {}'.format(exc_traceback.tb_lineno, exc_value, exc_type))

class GSheetColIncreFormatter(Formatter):
    def __init__(self):
        super().__init__()

    def format(self, record):
        col_it = record.idx_provider
        col_it.reset()
        sheet_name = record.name
        if type(record.msg) is list:
            keys = record.msg
        elif type(record.msg) is str:
            ## many lists of values
            keys = [ record.msg ]
        else:
            return None, None
        ## make sure the length of args is the same as keys
        if len(record.args) != len(keys):
            return None, None
        data = []
        clears = []
        for idx, val in enumerate(keys):
            col = col_it.next()
            range = "%s!%s%d:%s%d" % (sheet_name, col, 1, col, 1)
            ## header
            item = { "values": [ [val] ], "range": range, "majorDimension": "COLUMNS" }
            data.append(item)
            range = "%s!%s%d:%s%d" % (sheet_name, col, 2, col, len(record.args[idx]) + 1)
            v = record.args[idx]
            if has_numpy:
                if type(v) is numpy.array or type(v) is numpy.ndarray:
                    v = v.tolist()
            v = ['#N/A' if isnan(x) else x for x in v]
            item = { "values": [ v ], "range": range, "majorDimension": "COLUMNS" }
            data.append(item)

            item = "%s!%s:%s" % (sheet_name, col, col)
            clears.append(item)
        return data, clears
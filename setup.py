from distutils.core import setup

setup(
    name='gridlogger',
    version='0.2',
    description='A Python Logger, output to Google Sheets.',
    url='https://gitlab.com/halogot/google-spreadsheet-logger',
    author='Stephen Lau',
    author_email='register2@inode.serveftp.com',
    license='MIT',
    packages=['gridlogger'],
    zip_safe=False,
    install_requires=["numpy", "google-api-python-client", "httplib2"])

import logging
import unittest

from gridlogger.logging import GridLogHandler
from gridlogger.logging import GSheetColIncreFormatter

import config

class BasicTestSuite(unittest.TestCase):
    def test_hello_world(self):
        handler = GridLogHandler(config.sheetid, config.client_secret_json)
        formatter = GSheetColIncreFormatter()
        handler.setFormatter(formatter)
        logger = logging.getLogger("TESTENV")
        logger.setLevel(logging.INFO)
        logger.addHandler(handler)

        import random
        r1 = random.sample(range(100), 10)
        r2 = random.sample(range(100), 3)
        logger.info(['r1', 'r2'], r1, r2)

if __name__ == '__main__':
    unittest.main()

import logging
import unittest

from pprint import pprint
from logging import Handler
from logging import Formatter
from requests import put
from httplib2 import Http

from oauth2client import file, client, tools
from apiclient.discovery import build
from googleapiclient.errors import HttpError

import config

class BasicTestSuite(unittest.TestCase):
    def test_hello_world(self):
        SCOPES = ['https://www.googleapis.com/auth/spreadsheets','https://www.googleapis.com/auth/drive','https://www.googleapis.com/auth/drive.file']
        store = file.Storage('credentials.json')
        creds = store.get()
        if not creds or creds.invalid:
            flow = client.flow_from_clientsecrets(config.gapi_client_secret, SCOPES)
            creds = tools.run_flow(flow, store)
        gservice = build('sheets', 'v4', http=creds.authorize(Http()))
        range = "TESTENV!C1:C8"
        request = gservice.spreadsheets().values().get(spreadsheetId=config.sheetid, range=range)
        response = request.execute()
        pprint(response)

if __name__ == '__main__':
    unittest.main()

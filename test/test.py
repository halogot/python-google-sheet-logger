import logging
import unittest

from gridlogger import GridLogHandler
from gridlogger import GSheetColIncreFormatter

import config

class BasicTestSuite(unittest.TestCase):
    def test_hello_world(self):
        handler = GridLogHandler(config.sheetid, config.client_secret_json)
        formatter = GSheetColIncreFormatter()
        handler.setFormatter(formatter)
        logger = logging.getLogger("TESTENV")
        logger.setLevel(logging.INFO)
        logger.addHandler(handler)

        import random
        r1 = random.sample(range(100), 8)
        r2 = random.sample(range(100), 2)
        import numpy as np
        import math
        r3 = np.asarray([1.2,math.nan,5.9,5.1, math.nan, 5.1, 5.3])
        logger.info(['r1', 'r2', 'r3'], r1, r2, r3)

if __name__ == '__main__':
    unittest.main()

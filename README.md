# Prerequisites

* Python 3
* A Google account
* The [pip](https://pypi.python.org/pypi/pip) package management tool
* [Google Sheets API v4](https://developers.google.com/sheets/api/quickstart/python)

# Description

This is a Python Logger, output to Google Sheets.

This provide a pretty, but no the most convenient, way to debug number lists, matrices.

Performance is a major concern when using this library. Google Sheets updates are sent through HTTP. The processing time is significant.

Google OAuth is involved. It might open a browser to do the authentication. Local port 8080 must be available.

# Install

    pip install -U gridlogger

# Run Tests

Setup Google API keys and Google Sheet target, in config.py

    sheetid = ''
    client_secret_json = ''

Run tests

    python -m unittest discover

# Workbench

* Anaconda 4.1.1
* Python 3.5.2
* Visual Studio Code
* [Anaconda3 Docker](https://hub.docker.com/r/continuumio/anaconda3/)

# Example

See test/test.py

    from gridlogger.logging import GridLogHandler
    from gridlogger.logging import GSheetColIncreFormatter
    
    def test_hello_world():
        handler = GridLogHandler(sheetid, client_secret_json)
        formatter = GSheetColIncreFormatter()
        handler.setFormatter(formatter)
        logger = logging.getLogger("TESTENV")
        logger.setLevel(logging.INFO)
        logger.addHandler(handler)
    
        import random
        r1 = random.sample(range(100), 6)
        r2 = random.sample(range(100), 8)
        logger.info(['r1', 'r2'], r1, r2)
        
# Notes

* Application exits automatically if oauth2client credential is invalid
`
SystemExit: Authentication has failed: invalid_clientUnauthorized
`

* In Python 3, logging.Handler has threading.lock control, so it is not included in this library
